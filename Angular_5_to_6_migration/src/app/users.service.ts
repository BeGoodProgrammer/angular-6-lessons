import { Injectable } from '@angular/core';

@Injectable()
export class UsersService {

  public clickCount: number = 0;

  public creationDate: Date = new Date();

  constructor() { }

  public increase(){
    this.clickCount++;
  }

  public getUsers(): Array<any>{
    return [
      {firstName: 'Ivan', lastName: 'Ivanov'},
      {firstName: 'Petr', lastName: 'Petrov'},
      {firstName: 'Vasya', lastName: 'Hmuriy'},  
      {firstName: 'Vasya', lastName: 'Hmuriy'},  
      {firstName: 'Vasya', lastName: 'Hmuriy'}
    ];
  }
}
