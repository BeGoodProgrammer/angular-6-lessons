import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { UsersService } from './services/users.service';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './api/in-memory-data.service';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserFormComponent } from './forms/user-form/user-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserGroupsComponent } from './user-groups/user-groups.component';

import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { UserFormDeactivateGuard } from './guards/user-form-deactivate.guard';

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent},
  { 
    path: 'users/:id',
    component: UserFormComponent,
    canDeactivate: [UserFormDeactivateGuard]
  },
  { 
    path: 'userGroups', 
    component: UserGroupsComponent,
    canActivate: [AuthGuard]
  },
  { path: '', redirectTo: '/users', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserFormComponent,
    UserGroupsComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    
    RouterModule.forRoot(
      appRoutes
    ),

    // please, remove this when real api will be ready
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
