import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  public user: User;

  constructor() {    
    
  }

  ngOnInit() {
    this.user = {
      id: 1,
      firstName: "Иван",
      lastName: "Иванов"
    }
  }

}
