import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  public page: number;

  public collectionSize: number;

  public users: Array<any>;

  public itemsPerPage: number = 4;

  constructor(private usersService: UsersService) { 
    this.page = 1;
    this.loadPage();
  }

  ngOnInit() {
  }

  onPageChanged() {
    this.loadPage();
  }

  private loadPage(){
    this.usersService.getUsers(this.page, this.itemsPerPage)
    .subscribe(p => {
      debugger;
      this.users = p.rows;
      this.collectionSize = p.totalCount;
    });
  }
}
