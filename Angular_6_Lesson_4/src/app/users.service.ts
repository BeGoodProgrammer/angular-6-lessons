import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TablePage } from './table-page';

@Injectable()
export class UsersService {

  public clickCount: number = 0;

  public creationDate: Date = new Date();

  constructor(
    private httpClient: HttpClient
  ) { }

  private usersUrl = "api/users";

  public increase(){
    this.clickCount++;
  }

  public getUsers(page: number, itemsPerPage: number): Observable<TablePage>{
    var users = this.httpClient.get<any[]>(this.usersUrl);

    return this.getPageItems(users, page, itemsPerPage);
  }

  private getPageItems(users: Observable<Array<any>>, page: number, itemsPerPage: number): Observable<TablePage>
  {    
    return users.pipe(
      map(u => {
        var startIndex = itemsPerPage * (page -1);
        return new TablePage(u.length, u.slice(startIndex, startIndex + itemsPerPage));
      }
    ));   
  }
}
