import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public page: number;

  public anyPage: any;

  public collectionSize: number;

  public users: Array<User>;

  public itemsPerPage: number = 4;

  constructor(private usersService: UsersService) { 
    this.page = 1;

    this.loadPage();
  }

  ngOnInit() {
  }

  onPageChanged() {
    this.loadPage();
  }

  private loadPage(){
    this.usersService.getUsers(this.page, this.itemsPerPage)
    .subscribe(p => {
      this.users = p.rows;
      this.users[0].firstName = "";
      this.collectionSize = p.totalCount;
    });
  }
}
