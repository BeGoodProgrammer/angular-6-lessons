import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const users = [
      {firstName: 'Ivan', lastName: 'Ivanov'},
      {firstName: 'Petr', lastName: 'Petrov'},
      {firstName: 'Vasya', lastName: 'Hmuriy'},  
      {firstName: 'Vasya', lastName: 'Hmuriy'},  
      {firstName: 'Vasya', lastName: 'Hmuriy'},
      {firstName: 'Vasya2', lastName: 'Hmuriy2'}
    ];
    const users2 = [
      {firstName: 'Ivan', lastName: 'Ivanov'},      
    ];
    return { users, users2 }; 
  }

  constructor() { }
}
