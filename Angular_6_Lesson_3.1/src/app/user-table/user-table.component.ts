import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {

  public page: number;

  public collectionSize: number;

  public users: Array<any>;

  constructor(private usersService: UsersService) { 
    this.page = 1;
    usersService.getUsers()
      .subscribe(users => {
        this.users = users
        this.collectionSize = this.users.length;
      });
  }

  ngOnInit() {
  }

  onPageChanged(pageNumber){
    console.log("page changed:" + pageNumber);
  }
}
