import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ArgumentOutOfRangeError } from 'rxjs/internal/util/ArgumentOutOfRangeError';

import { Page } from './page';
import { User } from '../models/user';
import { map } from 'rxjs/operators';

@Injectable()
export class UsersService {

  public clickCount: number = 0;

  public creationDate: Date = new Date();

  constructor(
    private httpClient: HttpClient
  ) { }

  private usersUrl = "api/users";

  public increase(){
    this.clickCount++;
  }

  public getUsers(page: number, itemsPerPage: number): Observable<Page>{
    var users = this.httpClient.get<User[]>(this.usersUrl);
    return this.GetPageData(users, page, itemsPerPage);
  }

  private GetPageData(users: Observable<Array<User>>, page:number, itemsPerPage): Observable<Page>
  {
    if (page <= 0){
      throw Error("page number should be > 0");
    }

    var startIndex = itemsPerPage * (page - 1);    
    return users
    .pipe(
      map(data => {
        var slicedData = itemsPerPage > 0 
            ? data.slice(startIndex, startIndex + itemsPerPage)
            : data;
        return new Page(data.length, slicedData);
      }
    ));
  }  
}