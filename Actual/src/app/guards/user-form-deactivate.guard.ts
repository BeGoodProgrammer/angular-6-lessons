import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CanDeactivate } from '@angular/router';
import { UserFormComponent } from '../forms/user-form/user-form.component';

@Injectable({
  providedIn: 'root'
})
export class UserFormDeactivateGuard implements CanDeactivate<UserFormComponent> {
  canDeactivate(
    component: UserFormComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return !component.hasUnsavedData() || confirm('На странице есть несохраненные изменения. Продолжить?');     
  }
}
